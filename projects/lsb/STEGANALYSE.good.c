/*
 Dans cette stéganalyse, on suppose que :
 le texte (ASCII-128) a été caché comme suit : les 3 bits de poids faible du caractère on été cachés dans les trois bits de poids faible
                                   du composant bleu de l'image, puis les 2 bits suivant du caractère dans les 2 bits de poids
                                   faible du composant vert, puis les deux bits suivant dans les 2 bits de poids faible du 
                                   composant rouge.
                                   
 L'image RGB contenant "le texte caché" a été caché comme suit : 1 pixel sera caché en utilisant 2 pixels de l'image mère, ainsi :
                                  les 4 bits de poids fort du composant rouge (resp. vert, bleu) de l'image fille seront cachés 
                                  dans les 4 bits de poids faible du composant rouge (resp. vert, bleu) du pixel mère. Puis, les 4 bits 
                                  de poids faibles du composant rouge (resp. vert, bleu) de l'image fille seront ensuite cachés 
                                  dans les 4 bits de poids faible du composant rouge (resp. vert, bleu) du pixel suivant.
                                  
 L'image mère "globale" 300*250 s'appelle : tour_eiffel_stegano.bmp
 
 On suppose que vous avez le package SDL déjà installé sur votre machine, sinon il faudra l'installer.
 
 Le code a été largement commenté pour vous permettre de le comprendre plus facilement et plus rapidement.
 */



#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <SDL/SDL.h>


/*Fonction qui sert à charger une image*/
SDL_Surface * load_image( char * filename ) 
{
	SDL_Surface * Image_chargee = NULL; //Surface tampon qui nous servira pour charger l'image
	SDL_Surface * Image_optimisee = NULL; //L'image optimisée qu'on va utiliser

	Image_chargee = SDL_LoadBMP(filename); //Chargement de l'image BMP

	if( Image_chargee != NULL ) //Si le chargement se passe bien
	{ 	 
	    Image_optimisee = SDL_DisplayFormat(Image_chargee); /*Pour convertir l'image chargée vers le format 
	                                                       d'affichage adapté (celui de l'écran actuel)*/
	    SDL_FreeSurface(Image_chargee); //Libération de l'ancienne image chargée
	}
	return Image_optimisee; //On retourne l'image optimisée 
}



/* Pour applliquer une surface sur une autre à partir de la position en haut à gauche (x,y)*/
void appliquer_surface(int x, int y, SDL_Surface * source, SDL_Surface * destination) 
{ 
	SDL_Rect POS; /*Pour définir une zone rectangulaire commençant en haut à gauche*/
	POS.x = x; /*position x de depart en haut à gauche*/
	POS.y = y; /*position y de depart en haut à gauche*/
	SDL_BlitSurface(source, NULL, destination, &POS ); //On applique toute la surface source => destination
}



/* Fonction qui permet de recuperer la valeur d'un pixel d'une image, à une position (x,y) donnée. 
   Ce qui permettrait ensuite de recupérer les valeurs des composant RGB du pixel via la fonction SDL_GetRGB.*/
Uint32 getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel; /*Pour obtenir l'encodage des pixels de l'image*/
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp)
    {
    case 1:
        return *p;
 
    case 2:
        return *(Uint16 *)p;
 
    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2]; //L'octet le plus fort est mis à l'adresse mémoire la plus petite
        else
            return p[0] | p[1] << 8 | p[2] << 16; //L'octet le plus faible est mis à l'adresse mémoire la plus petite
    case 4:
        return *(Uint32 *)p;
 
    default:
        return 0;
    }
}



int B_FIRST_BIT = 15728640;
int B_LAST_BIT = 983040;
int V_FIRST_BIT = 61440;
int V_LAST_BIT = 3840;
int R_FIRST_BIT = 240;
int R_LAST_BIT = 15;

void definirPixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    /*nbOctetsParPixel représente le nombre d'octets utilisés pour stocker un pixel.
    En multipliant ce nombre d'octets par 8 (un octet = 8 bits), on obtient la profondeur de couleur
    de l'image : 8, 16, 24 ou 32 bits.*/
    int nbOctetsParPixel = surface->format->BytesPerPixel;
    /*Ici p est l'adresse du pixel que l'on veut modifier*/
    /*surface->pixels contient l'adresse du premier pixel de l'image*/
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * nbOctetsParPixel;
  
    /*Gestion différente suivant le nombre d'octets par pixel de l'image*/
    switch(nbOctetsParPixel)
    {
        case 1:
            *p = pixel;
            break;
  
        case 2:
            *(Uint16 *)p = pixel;
            break;
  
        case 3:
            /*Suivant l'architecture de la machine*/
            if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            {
                p[0] = (pixel >> 16) & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = pixel & 0xff;
            }
            else
            {
                p[0] = pixel & 0xff;
                p[1] = (pixel >> 8) & 0xff;
                p[2] = (pixel >> 16) & 0xff;
            }
            break;
  
        case 4:
            *(Uint32 *)p = pixel;
            break;
    }
}

/*Fonction qui recherche une image cachée dans une autre image*/
SDL_Surface * recherche_image_cachee(SDL_Surface * IMAGE_MERE, SDL_Surface * image_cachee)
{
  int i, x_f, x_m, y_m, bit_f, bit_m, larg_img_mere, hau_img_mere;
  int bpp = image_cachee->format->BytesPerPixel; /*Nombre d'octets/pixel de la surface de l'image cachée*/
  Uint32 PIXEL1, PIXEL2; /*PIXEL1 sera utilisé pour l'image mère et PIXEL2 pour l'image cachée*/
  Uint8 * p;                /*pointeur pour balayer et indexer les contenus des pixels de l'image cachée*/
  Uint8 R, G, B, r1, g1, b1;            /*composants R=>rouge, G=>vert et B=> bleu pour le pixel PIXEL1 de l'image mère*/
  larg_img_mere = IMAGE_MERE->w; /*Pour obtenir la largeur de l'image mère*/
  hau_img_mere = IMAGE_MERE->h;  /*Pour obtenir la hauteur de l'image mère*/
  
  for (y_m=0;y_m<hau_img_mere;y_m++)
  {
     for (x_m=0;x_m<larg_img_mere;x_m++)
     {
         PIXEL1=getpixel(IMAGE_MERE,x_m,y_m);
         SDL_GetRGB(PIXEL1, IMAGE_MERE->format, &R, &G, &B);
         b1 = ((B << 4) & 0xff);
         g1 = ((G << 4) & 0xff);
         r1 = ((R << 4 )& 0xff);
         R = 0x00000000;
         G= 0x00000000;
         B= 0x00000000;
         x_m++;
         PIXEL1=getpixel(IMAGE_MERE,x_m,y_m);
         SDL_GetRGB(PIXEL1, IMAGE_MERE->format, &R, &G, &B);
         r1 |= (R & 0xffff0000);
         g2 |= (G & 0xffff0000);
         b1 |= (B & 0xffff0000);

         PIXEL2=SDL_MapRGBA(image_cachee->format, r1, g1, b1, 255);
         definirPixel(image_cachee, x_m/2, y_m, PIXEL2);
     }
  } 
  return image_cachee;
}



/*Fonction qui recherche le texte caché dans l'image fille*/
char * recherche_texte_cache_dans_image(SDL_Surface * image, char * TEXTE)
{
  int i, j, x, y, caractere, bit_i, bit_c, larg_img, hau_img;
  Uint32 PIXEL;        /*PIXEL1 sera utilisé pour contenir la valeur du pixel en cours de l'image*/
  Uint8 R, G, B;       /*composants R=>rouge, G=>vert et B=> bleu pour le pixel PIXEL de l'image*/
  larg_img = image->w; /*Pour obtenir la largeur de l'image*/
  hau_img = image->h;  /*Pour obtenir la hauteur de l'image*/
  
  /*
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * A remplir
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   * 
   */
  
  return TEXTE;
}


/*Fonction qui affiche un texte sur la sortie standard*/
void affichage_texte(char * TEXTE, int T)
{
 int i;
 printf("----------------------------------------------------\n");
 for (i=0; i<T; i++)
 {
  printf("%c", TEXTE[i]);
 }
 printf("----------------------------------------------------\n");
}




int main (int argc, char * argv[])
{
  int LARGEUR_ECRAN, HAUTEUR_ECRAN, CODAGE, PAUSE;
  LARGEUR_ECRAN=800; HAUTEUR_ECRAN=400;
  CODAGE = 24; //8x3=24 bits / pixel couleur
  char * TEXTE;
  SDL_Surface * ecran = NULL;        //déclaration d'une surface d'écran de base (sur laquelle on va travailler)
  SDL_Surface * IMAGE = NULL;        //déclaration d'une surface (pour l'image qui cache)
  SDL_Surface * image_cachee = NULL; //Déclaration d'une autre surface (pour l'image cachée)
  

  SDL_Event event; //Pour gérer les evénements type clic de souris
  
  /*
  //if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 ) //Initialise la bibliothèque SDL ainsi que les sous-systèmes spécifiés 
  {
        printf( "Impossible d'initialiser SDL: %s\n", SDL_GetError( ) );
        return EXIT_FAILURE;
  }
  */
  
 
  ecran = SDL_SetVideoMode(LARGEUR_ECRAN, HAUTEUR_ECRAN, CODAGE, SDL_SWSURFACE); //Initialisation du mode video
  SDL_WM_SetCaption( "Steganalyse Image BMP", NULL ); //Mise en place de la barre caption nommée "Steganalyse Image BMP"
  
  IMAGE = load_image( "tour_eiffel_stegano.bmp" ); //Chargement de l'image mère contenant les informations cachées
  
  appliquer_surface(0, 0, IMAGE, ecran); //On applique l'image mère sur l'écran de base au coin haut gauche (0,0)
  
  SDL_Flip( ecran ); //pour mettre à jour l'écran et faire apparaitre la nouvelle surface appliquée juste avant
  
  
  PAUSE=1;
  while (PAUSE) 
  {
     SDL_WaitEvent(&event); //attend qu'un événement se produise
     switch(event.type) 
     {
      case SDL_MOUSEBUTTONUP: // en cas de Clic de la souris
         if(event.button.button==SDL_BUTTON_LEFT) 
	 {PAUSE=0;/*Pour ensuite sortir de la boucle et conituer le programme*/}
     } 
  }
  
  image_cachee = SDL_CreateRGBSurface(0, 150, 250, 24, 0, 0, 0, 0); //Création d'une surface RGB 150x250 (24 bits/pixel)
  
  image_cachee = recherche_image_cachee(IMAGE, image_cachee); //on cherche l'image cachée dans l'image mère
  
  printf("L'image cachée est la suivante :\n");
  appliquer_surface(350, 0, image_cachee, ecran); //On applique l'image sur l'écran à la position (y=0 et x=350)
  SDL_Flip(ecran); //pour mettre à jour l'écran et faire apparaitre la nouvelle surface appliquée précédemment
  
  PAUSE=1;
  while (PAUSE) /*Pour faire une pause sur l'écran et observer */
  {
     SDL_WaitEvent(&event); //attend qu'un événement se produise
     switch(event.type) 
     {
      case SDL_MOUSEBUTTONUP: // en cas de Clic de la souris
         if(event.button.button==SDL_BUTTON_LEFT) //Si on clique sur le bouton gauche de la souris 
	 {PAUSE=0;}
     } 
  }
  
  /*Recherche puis affichage du texte caché dans une image*/
  TEXTE = malloc(sizeof(char)*37500);
  TEXTE = recherche_texte_cache_dans_image(image_cachee, TEXTE);
  printf("Le texte caché est le suivant :\n");
  affichage_texte(TEXTE, 500);
  //affichage_texte(TEXTE, 236);


  SDL_FreeSurface(IMAGE);
  SDL_FreeSurface(image_cachee);
  SDL_FreeSurface(ecran);
  free(TEXTE);
  SDL_Quit();
  
  return EXIT_SUCCESS;
}











































