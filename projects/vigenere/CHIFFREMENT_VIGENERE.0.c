/*******************************************************************************/
/****                                                                        ***/
/****                    CHIFFREMENT DE VIGENERE                             ***/
/****                                                                        ***/
/*******************************************************************************/

#include "LIBRAIRIES.h"
#include "CHIFFREMENT_VIGENERE.h"
#include "FONCTIONS_COMMUNES.h"


/******************** Fonction de Chiffrement Vigenere **********************/
void Chiffrement_vigenere(int * cle, int longueur_cle,  int * texte_clair, int * texte_chiffre, int longueur_texte)
{
/*
  texte_clair contient le message en clair
  texte_chiffre va contenir le message chiffré
*/

  int i, j, nb_blocks;
 
  if(longueur_cle > 0)
  {
      nb_blocks = longueur_texte/longueur_cle;
      for(i=0;i<nb_blocks;i++)
      {
	       for(j=0;j<longueur_cle;j++)
	       {
            texte_chiffre[i * longueur_cle] += cle[j] % 25;          
	         /*
	         A decommenter puis remplir
	          texte_chiffre[??????]=??????;
	         */
	       }
      }
      for(j=0;j<longueur_texte-nb_blocks*longueur_cle;j++)
      {
	      /*
	        A decommenter puis remplir
	        texte_chiffre[??????]=??????;
	      */
      }
  }
  else /*gestion de l'erreur de la non positivité de la clé.*/
  {
      printf("\nErreur : Usage longueur_cle est un entier strictement positif \n"); 
  }
}




/******************** Fonction de Déchiffrement Vigenere **********************/
void Dechiffrement_vigenere( int * cle, int longueur_cle,  int * texte_chiffre, int * texte_clair_2, int longueur_texte)
{
/*
  texte_chiffre contient le message chiffré
  texte_clair_2 va contenir le message dechiffré
*/

  int i, j, nb_blocks;
  
  if( longueur_cle > 0)
  {
      nb_blocks = longueur_texte/longueur_cle;
      for(i=0;i<nb_blocks;i++)
      {
	     for(j=0;j<longueur_cle;j++)
	     {
          texte_clair_2[i * longueur_cle] -= cle[j] % 25;

	      /*
	        A decommenter puis remplir
	        texte_clair_2[??????]=??????;
	      */ 
	     }
      }
      for(j=0;j<longueur_texte-nb_blocks*longueur_cle;j++)
      {
	      /*
	        A decommenter puis remplir
	        texte_clair_2[??????]=??????;
	      */
      }
  }
  else /*gestion de l'erreur de la non positivite de la clef.*/
  {
      printf("\nErreur : Usage  longueur_cle est un entier strictement positif \n"); 
  }
}