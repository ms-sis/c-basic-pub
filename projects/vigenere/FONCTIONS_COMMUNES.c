#include "LIBRAIRIES.h"
#include "FONCTIONS_COMMUNES.h"


/***********            Fonction pour déterminer la longueur du texte             ************/
/* ATTENTION : 
 * le texte doit comporter uniquement les lettres de l'alphabet. 
 * Il ne faut pas aussi comptabiliser les retours à la ligne */

void Determination_long_texte(int * LT, char * nom_fichier)
{
  int lettre; /*variable pour sctocker les lettres du fichier "nom_fichier"*/
  int L=0;    /*Longueur du texte*/
  FILE * FIC;
  char c;
  if ( (FIC = fopen(nom_fichier, "r")) )  {
    while((lettre = fgetc(FIC)) != EOF) {
      if ( (lettre >= 'a' && lettre <= 'z') || (lettre >= 'A' && lettre <= 'Z')) {
        L++;
      }
    }
  }
  else {
    printf("Could not open file in read mode \n");
    abort();
  }

  printf("Debug / Determination_long_texte > %d \n", L);

  *LT = L; /*on met à jour le contenu de LT qui est la longueur du texte*/
  fclose(FIC); /* Fermeture du fichier */
}




/***********            Fonction de Lecture/chargement du texte                ************/
/*on charge le texte provenant du fichier "nom_fichier" dans le tableau texte*/
void Lire_et_charger_texte(int * texte, int longueur_texte, char * nom_fichier)
{
  int i, lettre;
  i=0;
  FILE * FIC;
  if (!texte || texte == NULL) {
    printf("Invalid arguments \n");
    abort();
  }

  if ( (FIC = fopen(nom_fichier, "r")) )  {
    while((lettre = fgetc(FIC)) != EOF) {
      if (lettre >= 'a' && lettre <= 'z') {
        texte[i]=lettre - ((int)'a');
        i++;
      }
      else if (lettre >= 'A' && lettre <= 'Z') {
        texte[i]=lettre - ((int)'A');
        i++;
      }
    }
  }
  else {
    printf("Could not open file in read mode \n");
    abort();
  }

  fclose(FIC); /* Fermeture du fichier */
}


/******************* Ecriture du Texte Chiffré ***************************/
void Ecrire_chiffre(int * texte_chiffre, int longueur_texte, char * nom_fichier)
{
  FILE * FIC;
  int i = 0;

  if ( (FIC = fopen(nom_fichier, "w"))) {
    for(i=0;i<longueur_texte;i++)
    {
      fputc((texte_chiffre[i] + ((int)'a')), FIC);
    }
  }
  else {
    printf("Could not open file in read mode \n");
    abort();
  }

  fclose(FIC); /* Fermeture du fichier */
}



