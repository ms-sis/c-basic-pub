Travail à faire pour cette séance :


PARTIE A (chiffrement/déchiffrement par substitution => 10 pts)
C'est un chiffrement par substitution dont les lettres sont substituées par paire, exemple : a est substituée par h, et h est substituée par a, b par m et m par b, etc.

Avant de déchiffrer un message dont la clé est connue et donnée dans le fichier "cle1.txt", il faut d'abord :

1. Remplir le corps de la fonction Lire_cle(int cle[26], char * fichier_cle) se trouvant dans le fichier FONCTIONS_COMMUNES.c

2. Remplir les fonctions Chiffrer_substitution/Dechiffrer_substitution  dans le fichier "CHIFFREMENT.c". Puis déchiffrer le fichier "chiffre1.txt" en utilisant la clé contenue dans le fichier "cle1.txt". Pour cela, il suffit juste de suivre les instructions (choix D) du programme et de répondre au fur et à mesure.
NB : si le texte clair obtenu n'a aucun sens, ce que le déchiffrement s'est mal passé, dans ce cas revoyez votre code.




PARTIE B (cryptanalyse => 10 pts)
(le but ici est de déchiffrer le message contenu dans le fichier "chiffre2.txt"). Évidemment, on ne possède pas la clé, donc à vous de la trouver.

1. Dans le fichier "CRYPTANALYSE.c" :
 - Remplir le corps de la fonction Compter_lettre
 - Remplir les ?????? dans la fonction Comptage_nb_apparition
 - Remplir les ?????? dans la fonction Comptage_frequence_apparition
 - Remplir le corps de la fonction Compter_bigramme
 - Remplir les ?????? dans la fonction Affiche_clair
 - Remplir les ?????? dans la fonction Affiche_alphabetique

2. Lancer la cryptanalyse (choix C) et suivre les instructions afin de retrouver la clé et le message original, liés au fichier chiffré "chiffre2.txt".

Pour vous aider sur la cryptanalyse :

Voici un exemple sur 10000 lettres, du nombre d'apparition de quelques bigrammes :

es => 305
//le => 246
en => 242
de => 215
re => 209
nt => 197
on => 164
er => 163
te => 163
se => 155
et => 143
el => 141
qu => 134
ne => 124
ou => 118
ai => 117
em => 113
it => 112
me => 104
is => 103
la => 101
ec => 100
ti => 98
ce => 98
ed => 96
ie => 94
ra => 92
in => 90
...
q = e
y = s
r = l

z = n
a = d

a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z  
r  *  *  *  v  w  p  *  q  *  *  z  *  *  *  g  i  a  *  y  *  e  f  *  t  l  

a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z  
r  o  s  k  v  w  p  x  q  n  d  z  u  j  b  g  i  a  c  y  m  e  f  h  t  l  

gwynplaineetaitsaltimbanqueilsefaisaitvoirenpublicpasdeffetcomparableausienilguerissaitleshypocondriesrienquensemontrantiletaitaeviterpourdesgensendeuilconfusetforcessilslapercevaientderireindecemmentunjourlebourreauvintetgwynplainelefitrireonvoyaitgwynplaineonsetenaitlescotesilparlaitonseroulaitaterreiletaitlepoleopposeduchagrinspleenetaitaunboutetgwynplainealautreaussietaitilparvenurapidementdansleschampsdefoireetdanslescarrefoursaunefortsatisfaisanterenommeedhommehorriblecestenriantquegwynplainefaisaitrireetpourtantilneriaitpassafaceriaitsapenseenonlespecedevisageinouiquelehasardouuneindustriebizarrementspecialeluiavaitfaconneriaittoutseulgwynplainenesenmelaitpasledehorsnedependaitpasdudedanscerirequilnavaitpointmissursonfrontsursesjouessursessourcilssursaboucheilnepouvaitlenoteronluiavaitajamaisappliqueleriresurlevisagecetaitunrireautomatiqueetdautantplusirresistiblequiletaitpetrifiepersonnenesederobaitacerictusdeuxconvulsionsdelabouchesontcommunicativeslerireetlebaillementparlavertudelamysterieuseoperationprobablementsubiepargwynplaineenfanttouteslespartiesdesonvisagecontribuaientacerictustoutesaphysionomieyaboutissaitcommeuneroueseconcentresurlemoyeutoutessesemotionsquellesquellesfussentaugmentaientcetteetrangefiguredejoiedisonsmieuxlaggravaientunetonnementquilauraiteuunesouffrancequilauraitressentieunecolerequiluiseraitsurvenueunepitiequilauraiteprouveeneussentfaitquaccroitrecettehilaritedesmusclessileutpleureileutrietquoiquefitgwynplainequoiquilvoulutquoiquilpensatdesquillevaitlatetelafoulesilafouleetaitlaavaitdevantlesyeuxcetteapparitionleclatderirefoudroyantquonsefigureunetetedemedusegaietoutcequonavaitdanslespritetaitmisenderouteparcetinattenduetilfallaitrire


