

int getSBoxValue(int num);
int getSBoxInvert(int num);
void KeyExpansion();
void AddRoundKey(int round);
void SubBytes();
void InvSubBytes();
void ShiftRows();
void InvShiftRows();
void MixColumns();
void InvMixColumns();
void MixBits(int round, double graine);
void InvMixBits(int round, double graine);
void Cipher();
void InvCipher();
void Charger_texte_par_bloc_de_16carac(unsigned char * texte, FILE * FIC);