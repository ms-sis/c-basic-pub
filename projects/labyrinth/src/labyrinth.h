#include "dependencies.h"

typedef enum {
    DEFAULT = 61440,
	NORTH = 1,
    EAST = 2,
	SOUTH = 3,
	WEST = 4
} Wall;

// #region Labyrinth data
typedef unsigned short int Cell;
typedef Cell** LMatrix;

struct point {
    int x;
    int y;
};
typedef struct point Point;

struct labyrinth {
    LMatrix matrix;
    int width;
    int height;
    Point* exit;
    Point* entry;
    Point* walker;
} labyrinth;
typedef struct labyrinth Labyrinth;
typedef void (*Action)();
// #endregion

// #region Labyrinth methods
// public methods
Labyrinth* labyrinth_unserialize(char* serializedFilePath);
Labyrinth* labyrinth_new(int width, int height, Action renderer); //, ...);
void reset(Labyrinth* this);
void generate_random(Labyrinth* this, Action renderer);
void generate_perfect(Labyrinth* this, Action renderer); //, ...);
int is_walker_cell(Labyrinth* this, Cell* cell);
int is_entry_cell(Labyrinth* this, Cell* cell);
int is_exit_cell(Labyrinth* this, Cell* cell);
int is_visited_cell(Labyrinth* this, Cell* cell);
int has_wall(Labyrinth* this, Cell* cell, Wall type);

int solve(Labyrinth* this, int sleep_timeout, int max_round, Action renderer);
Cell* move_next_cell(Labyrinth* this, int origin_x, int origin_y, Cell** last_cell_holder);
void move_walker_to_direction(Labyrinth* this, Wall direction);
int is_dead_end(Labyrinth* this, int x, int y);
int count_available_path(Labyrinth* this, Cell* cell);
int increment_cell_weight(Cell* cell);
int set_cell_weight(Cell* cell, int weight);
int get_cell_weight(Cell* cell);
Cell* cell_at_xy(Labyrinth* this, int x, int y);
Cell* cell_at_direction(Labyrinth* this, int origin_x, int origin_y, Wall direction);
void labyrinth_destroy(Labyrinth* labyrinth);

// private methods
int _has_north_wall(Cell* cell);
int _has_east_wall(Cell* cell);
int _has_south_wall(Cell* cell);
int _has_west_wall(Cell* cell);
void _link_cell_to_labyrinth(Cell* cell);
int _pick_rnd_unlinked_cell_flattened_pos(Labyrinth* this, int flatten_pos, Wall* direction);
int _has_unlinked_siblings(Labyrinth* this, int flatten_pos);
int _is_cell_linked_to_labyrinth(Cell* cell);
Cell* _get_cell_at_xy_from_flattened_pos(Labyrinth* this, int flatten_pos);
void _get_xy_from_flattened_matrix(Labyrinth* this, int flatten_pos, int* out_x, int* out_y);
int _get_flattened_pos_from_xy(Labyrinth* this, int x, int y);
void _break_random_wall(Labyrinth* this,int x, int y);
void _break_wall_at_flatten(Labyrinth* this, int flatten_pos, Wall direction);
void _break_wall_at(Labyrinth* this, int x, int y, int direction);
int _random(int max);
// #endregion
