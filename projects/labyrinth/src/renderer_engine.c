#include "dependencies.h"

#define TOP_WALL_ASSET ("--------------------------------------------------------------------------------------------------")
#define EMPTY_ASSET ("                                                                                                    ")

void console_labyrinth_renderer(Labyrinth* labyrinth) {
    if (!labyrinth || NULL == labyrinth) {
        throw_exception("InvalidArgumentException : Labyrinth");
    }

    system("clear");
    for(int y=0; y<=labyrinth->height; ++y) {
        _render_row_walls(labyrinth, y);
    }
}

// Draw left and top wall for each cells
// Draw right wall for cells when x = n (aka labyrinth width)
// Draw bottom wall for cells when y = m (aka labyrinth height)
void _render_row_walls(Labyrinth *labyrinth, int y) {
    int cell_size = 3;

    for(int i=0; i<cell_size; ++i) {
        for (int x=0; x<=labyrinth->width; ++x) {
            Cell* cell = &labyrinth
                            ->matrix
                                [x == labyrinth->width ? x - 1 : x]
                                [y == labyrinth-> height ? y - 1 : y];
            Cell* west_cell = &labyrinth
                                ->matrix
                                    [x > 0 ? x - 1 : x]
                                    [y];

            // Draw east wall for cells on x = n
            if (x == labyrinth->width) {
                if (has_wall(labyrinth, cell, EAST)) {
                    if (i == 0) {
                        printf("+");
                    }
                    else if (i <= cell_size / 2 && y != labyrinth->height) {
                        printf("|");
                    }
                }
                continue;
            }

            // Draw south wall for cells on y = m
            if (y == labyrinth->height) {
                if (has_wall(labyrinth, cell, SOUTH) && i == 0) {
                    printf("+%.*s", cell_size, TOP_WALL_ASSET);
                }
                continue;
            }

            // Draw other cell walls
            if (i == 0) {
                int cell_width = cell_size + 1;

                if (has_wall(labyrinth, cell, WEST) || 
                    has_wall(labyrinth, cell, NORTH) ||
                    has_wall(labyrinth, west_cell, NORTH)) {
                    printf("+");
                    cell_width--;
                }

                if (has_wall(labyrinth, cell, NORTH)) {
                    printf("%.*s", cell_width, TOP_WALL_ASSET);
                }
                else {
                    printf("%.*s", cell_width, EMPTY_ASSET);
                }

            }
            else if (i <= (cell_size/2)) 
            {
                int cell_width = cell_size + 1;

                if (has_wall(labyrinth, cell, WEST)) {
                    printf("|");
                    cell_width--;
                }

                if (i == (ceil((double)cell_size/4)) )
                {
                    if (is_walker_cell(labyrinth, cell)) {
                        _render_cell_special_state(cell, cell_width, WALKER);
                    }
                    else if (get_cell_weight(cell) == 2047) {
                         _render_cell_special_state(cell, cell_width, DEAD_END);
                    }
                    else if (is_entry_cell(labyrinth, cell)) {
                        _render_cell_special_state(cell, cell_width, ENTRY);
                    }
                    else if (is_exit_cell(labyrinth, cell)) {
                        _render_cell_special_state(cell, cell_width, EXIT);
                    }
                    else if(is_visited_cell(labyrinth, cell)) {
                        _render_cell_special_state(cell, cell_width, VISITED);
                    }
                    else {
                        printf("%.*s", cell_width, EMPTY_ASSET);
                    }
                }
                else {
                    printf("%.*s", cell_width, EMPTY_ASSET);
                }
            }
        }

        if (i <= cell_size / 2) {
            printf("\n");
        }
    }
}

void _render_cell_special_state(Cell* cell, int cell_width, SpecialState state) {

    printf("%.*s", cell_width / 2, EMPTY_ASSET);
    switch(state) {
        case WALKER: 
            printf("⚉%.*s", cell_width % 2, EMPTY_ASSET);
        break;
        case DEAD_END:
            printf("☓%.*s", cell_width % 2, EMPTY_ASSET);
        break;
        case ENTRY: 
            printf("E%.*s", cell_width % 2, EMPTY_ASSET);
        break;
        case EXIT:
            printf("⚑%.*s", cell_width % 2, EMPTY_ASSET);
        break;
        case VISITED: {
            int cell_weight = get_cell_weight(cell);
            switch(cell_weight) {
                case 0:
                case 1:
                    printf("⚫%.*s", cell_width % 2, EMPTY_ASSET);
                    break;
                case 2:
                    printf("\e[1;36m⚫%.*s\e[0;00m", cell_width % 2, EMPTY_ASSET);
                break;
                case 3:
                    printf("\e[1;35m⚫%.*s\e[0;00m", cell_width % 2, EMPTY_ASSET);
                break;
                case 4:
                    printf("\e[1;34m⚫%.*s\e[0;00m", cell_width % 2, EMPTY_ASSET);
                break;
                case 5:
                    printf("\e[1;33m⚫%.*s\e[0;00m", cell_width % 2, EMPTY_ASSET);
                break;
                case 6:
                    printf("\e[1;32m⚫%.*s\e[0;00m", cell_width % 2, EMPTY_ASSET);
                break;
                default:
                    printf("\e[1;31m⚫%.*s\e[0;00m", cell_width % 2, EMPTY_ASSET);
                    break;
            }
            
        }
        break;
        default:
            printf("⠎%.*s", cell_width % 2, EMPTY_ASSET);
        break;
    }
    printf("%.*s", cell_width / 2 - 1 , EMPTY_ASSET);
}