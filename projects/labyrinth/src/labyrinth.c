#include "dependencies.h"

#define MAX_CELL_WEIGHT 2047

Labyrinth* labyrinth_unserialize(char* serializedFilePath) {
    if (!serializedFilePath || serializedFilePath == NULL) { 
        throw_exception("InvalidArgumentException : serializedFilePath");
    }

    FILE* fp = NULL;
    if ( (fp = fopen(serializedFilePath, "r")) == NULL) {
        throw_exception("IOException: can not open serialized file in read mode");
    }

    const char separator[2] = " ";
    char* string_buffer = calloc(4096, sizeof(char));
    Labyrinth* labyrinth = NULL;
    int l_width = 0, 
        l_height = 0, 
        l_entry_x = 0, 
        l_entry_y = 0, 
        l_exit_x = 0, 
        l_exit_y = 0;

    for(int readed_line = 0; fgets(string_buffer, 4096, fp) != NULL; ++readed_line) {
        if(readed_line - 1 >= l_height) {
            break;
        }

        // The first line of serialized labyrinth contains metadata like width, height, enrty & Co
        if (readed_line == 0) {
            if (sscanf(string_buffer, "%d %d %d %d %d %d", 
                    &l_width, &l_height, &l_entry_x, &l_entry_y, &l_exit_x, &l_exit_y)
                != 6 ||
                (l_entry_x < 0 || l_entry_x > l_width - 1 ) ||
                (l_entry_y < 0 || l_entry_y > l_height - 1) || 
                (l_exit_x < 0 || l_exit_x > l_width - 1) || 
                (l_exit_y < 0 || l_exit_y > l_height - 1 )) {

                free(string_buffer);
                fclose(fp);
                throw_exception("\nSerializerException: invalid file format > invalid labyrinth metadata");
            }
            labyrinth = labyrinth_new(
                            l_width, 
                            l_height,
                            NULL
                        );
            labyrinth->entry->x = l_entry_x;
            labyrinth->entry->y = l_entry_y;
            labyrinth->walker->x = l_entry_x;
            labyrinth->walker->y = l_entry_y;
            labyrinth->exit->x = l_exit_x;
            labyrinth->exit->y = l_exit_y;
            continue;
        }

        char* token = strtok(string_buffer, separator);
        for(int col_pos = 0; token != NULL; ++col_pos) {
            if(col_pos >= l_width) {
                break;
            }

            Cell parsed_cell = (Cell) strtoul(token, NULL, 0);
            labyrinth->matrix[col_pos][readed_line - 1] = parsed_cell;

            token = strtok(NULL, separator);
        }
    }
    
    free(string_buffer);
    if (fclose(fp) != 0) {
        labyrinth_destroy(labyrinth);
        throw_exception("IOException: can not close file\n"); 
    }

    return labyrinth;
}

Labyrinth* labyrinth_new(int width, int height, Action renderer) {
    if  (width < 5 || height < 5) 
    { throw_exception("InvalidArgumentException : invalid labyrinth parameters"); }       

    // Create labyrint attributes
    Point* entry = calloc(1, sizeof(Point));
    Point* exit = calloc(1, sizeof(Point));
    Point* walker = calloc(1, sizeof(Point));

    entry->x = 0;
    entry->y = 0;
    walker->x = 0;
    walker->y = 0;
    exit->x = 0;
    exit->y = 0;

    // Init labyrinth
    LMatrix matrix = (LMatrix) calloc(width, sizeof(Cell*));
    for(int i=0; i<width; ++i) {
        matrix[i] = (Cell*) calloc(height, sizeof(Cell));
    }
    Labyrinth* self = calloc(1, sizeof(Labyrinth));
    self->matrix = matrix;
    self->width = width;
    self->height = height;
    self->entry = entry;
    self->exit = exit;
    self->walker = walker;

    reset(self);
    if (NULL != renderer) {
        renderer(self);
    }

    return self;
}

void reset(Labyrinth* this) {
    if (!this || this == NULL) { throw_exception("InvalidArgumentException: Labyrinth*"); }

    for(int i=0; i<this->width; ++i) { 
        for(int j=0; j<this->height; ++j) {
            this->matrix[i][j] = DEFAULT;
        }
    }
}

int is_walker_cell(Labyrinth* this, Cell* cell) {
    if (!this || this == NULL) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    if (!cell || NULL == cell) { throw_exception("InvalidArgumentException: Cell*"); }

    Cell* walker_cell = &this->matrix
                            [this->walker->x]
                            [this->walker->y];
    return walker_cell == cell;
}

int is_entry_cell(Labyrinth* this, Cell* cell) {
    if (!this || this == NULL) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    if (!cell || NULL == cell) { throw_exception("InvalidArgumentException: Cell*"); }

    Cell* enrty_cell = &this->matrix
                            [this->entry->x]
                            [this->entry->y];

    return enrty_cell == cell;
}

int is_exit_cell(Labyrinth* this, Cell* cell) {
    if (!this || this == NULL) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    if (!cell || NULL == cell) { throw_exception("InvalidArgumentException: Cell*"); }

    Cell* exit_cell = &this->matrix
                            [this->exit->x]
                            [this->exit->y];

    return exit_cell == cell;
}

int is_visited_cell(Labyrinth* this, Cell* cell) {
    if (!this || this == NULL) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    return get_cell_weight(cell);
}

int has_wall(Labyrinth* this, Cell* cell, Wall type) {
    if (!this || this == NULL) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    if (!cell || cell == NULL) { throw_exception("InvalidArgumentException: Cell*"); }
    if (!type) { throw_exception("InvalidArgumentException: Wall"); }

    switch(type) {
        case NORTH:
            return _has_north_wall(cell);
        case EAST:
            return _has_east_wall(cell);
        case SOUTH:
            return _has_south_wall(cell);
        case WEST:
            return _has_west_wall(cell);
    }
}

int _has_north_wall(Cell* cell) {
    return (*cell & (1 << 15)) ? 1: 0;
}

int _has_east_wall(Cell* cell) {
    return (*cell & (1 << 14)) ? 1 : 0;
}

int _has_south_wall(Cell* cell) {
    return (*cell & (1 << 13)) ? 1 : 0;
}

int _has_west_wall(Cell* cell) {
    return (*cell & (1 << 12)) ? 1 : 0;
}

// #region generator
void generate_random(Labyrinth* this, Action renderer) {
    if (!this || NULL == this) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    reset(this);

    // bearks random walls
    int wall_to_break_count = this->width * this->height * 2;
    for (int i = 0; i<wall_to_break_count; ++i) {
        _break_random_wall(this, _random(this->width), _random(this->height));
        if (NULL != renderer) {
            renderer(this);
            sleep(1);
        }
    }

    // rebuild edge walls
    for(int x=0; x < this->width; ++x) {
        Cell* north_cell = &this->matrix[x][0];
        Cell* south_cell = &this->matrix[x][this->height - 1];

        *north_cell |= (1<<15);
        *south_cell |= (1 << 13);

        for (int y=0; y < this->height; y++) {
            Cell* east_cell = &this->matrix[this->width - 1][y];
            Cell* west_cell = &this->matrix[0][y];

            *east_cell |= (1 << 14);
            *west_cell |= (1 << 12);
        }

        if (NULL != renderer) {
            renderer(this);
            sleep(1);
        }
    }

    // find entry
    do {
        this->entry->x = _random(this->width);
        this->entry->y = _random(this->height);
        this->walker->x = this->entry->x;
        this->walker->y = this->entry->y;
    }
    while(count_available_path(
        this,
        cell_at_xy(
            this,
            this->entry->x,
            this->entry->y))
        != 3);

    //find exit
    do {
        this->exit->x = _random(this->width);
        this->exit->y = _random(this->height);
    }
    while(count_available_path(
        this,
        cell_at_xy(
            this,
            this->exit->x,
            this->exit->y))
        != 3);

    if (NULL != renderer) {
        renderer(this);
    }
}


void generate_perfect(Labyrinth* this, Action renderer) {
    if (!this || NULL == this) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    reset(this);

    int* linked_cells_flattened_position_history = (int*) calloc(this->width * this->height, sizeof(int));
    int history_cursor = 0;

    linked_cells_flattened_position_history[history_cursor] 
            =  _get_flattened_pos_from_xy(this, _random(this->width), _random(this->height));

    do 
    {
        int current_cell_flatten_pos = linked_cells_flattened_position_history[history_cursor];
        _link_cell_to_labyrinth(
            _get_cell_at_xy_from_flattened_pos(
                this, 
                current_cell_flatten_pos));

        if (_has_unlinked_siblings(this, current_cell_flatten_pos)) {

            Wall direction = NORTH;
            int next_cell_flatten_pos = _pick_rnd_unlinked_cell_flattened_pos(
                                                this, current_cell_flatten_pos, &direction);

            _break_wall_at_flatten(this, current_cell_flatten_pos, direction);
            linked_cells_flattened_position_history[++history_cursor] = next_cell_flatten_pos;
            if (NULL != renderer) {
                renderer(this);
                sleep(1);
            }
        }
        else {
            history_cursor--;
        }
    }
    while(history_cursor > 0);
    free(linked_cells_flattened_position_history);

    // set random entry
    this->entry->x = _random(this->width);
    this->entry->y = _random(this->height);
    this->walker->x = this->entry->x;
    this->walker->y = this->entry->y;

    // set random exit
    this->exit->x = _random(this->width);
    this->exit->y = _random(this->height);
}

void _link_cell_to_labyrinth(Cell* cell) {
    *cell |= (1 << 11);
}

int _pick_rnd_unlinked_cell_flattened_pos(Labyrinth* this, int flatten_pos, Wall* direction) {
    Cell* sibling = NULL;
    int x, y;
    _get_xy_from_flattened_matrix(this, flatten_pos, &x, &y);

    while(sibling == NULL || _is_cell_linked_to_labyrinth(sibling)) {
        *direction = _random(4) + 1;
        sibling = cell_at_direction(this, x, y, *direction);
    }

    switch(*direction) {
        case NORTH:
            y--;
        break;
        case EAST:
            x++;
        break;
        case SOUTH:
            y++;
        break;
        case WEST:
            x--;
        break;  
    }

    return _get_flattened_pos_from_xy(this, x, y);
}

int _has_unlinked_siblings(Labyrinth* this, int flatten_pos) {
    int x, y;
    _get_xy_from_flattened_matrix(this, flatten_pos, &x, &y);

    for(int direction=NORTH; direction<=WEST; ++direction) {
        Cell* sibling = cell_at_direction(this, x, y, direction);
        if (NULL != sibling) {
            if (!_is_cell_linked_to_labyrinth(sibling)) {
                return 1;
            }
        }
    }
    return 0;
}

int _is_cell_linked_to_labyrinth(Cell* cell) {
    return *cell & (1 << 11);
}

Cell* _get_cell_at_xy_from_flattened_pos(Labyrinth* this, int flatten_pos) {
    int x, y;
    _get_xy_from_flattened_matrix(this, flatten_pos, &x, &y);

    return cell_at_xy(this, x, y);
}

void _get_xy_from_flattened_matrix(Labyrinth* this, int flatten_pos, int* out_x, int* out_y) {
    *out_x = flatten_pos / this->width;
    *out_y = flatten_pos % this->width;
    //http://codereview.stackexchange.com/questions/44653/turning-1d-array-to-2d-array-in-tictactoe
    //matrix1D[row * width + col] = matrix2D[row][col];
    //matrix2D[i / width][i % width] = matrix1D[i];
}

int _get_flattened_pos_from_xy(Labyrinth* this, int x, int y) {
    return x * this->width + y;
}

void _break_random_wall(Labyrinth* this,int x, int y) {
    _break_wall_at(this, x, y, _random(4)+1);
}

void _break_wall_at_flatten(Labyrinth* this, int flatten_pos, Wall direction) {
    int x,y;
    _get_xy_from_flattened_matrix(this, flatten_pos, &x, &y);

    _break_wall_at(this, x, y, direction);
}


void _break_wall_at(Labyrinth* this, int x, int y, int direction) {
    Cell* cell = &this->matrix[x][y];

    switch(direction) {
        case NORTH: 
            *cell &= ~(1<<15);
            if (y > 0) {
                Cell* north_cell = &this->matrix[x][y - 1];
                *north_cell &= ~(1<<13);
            }
        break;
        case EAST:
            *cell &= ~(1<<14);
            if (x < this->width - 1) {
                Cell* east_cell = &this->matrix[x + 1][y];
                *east_cell &= ~(1<<12);
            }
        break;
        case WEST:
            *cell &= ~(1<<12);
            if (x > 0) {
                Cell* west_cell = &this->matrix[x - 1][y];
                *west_cell &= ~(1<<14); 
            }
        break;
        default:
            *cell &= ~(1<<13);
            if (y < this->height - 1) {
                Cell* south_cell = &this->matrix[x][y + 1];
                *south_cell &= ~(1<<15);
            }
        break;
    }
}

int _random(int max) {
    return rand() % max;
}
// #endregion

// --------------------------------------------------------------------------
// #region solver
int solve(Labyrinth* this, int sleep_timout, int max_round, Action renderer) {
    if(!this || NULL == this) { throw_exception("InvalidArgumentException: Labyrinth*"); }

    int round = 0;
    Cell* walker_cell = cell_at_xy(this, this->walker->x, this->walker->y);
    Cell* exit_cell = cell_at_xy(this, this->exit->x, this->exit->y);
    Cell** last_cell_holder = calloc(1, sizeof(Cell*));
    do {
        round++;
        increment_cell_weight(walker_cell);
        if (NULL != renderer) {
            renderer(this);
        }
        sleep(sleep_timout);
    }
    while( (walker_cell = move_next_cell(
                            this, 
                            this->walker->x, 
                            this->walker->y,
                            last_cell_holder
                            )
            ) != exit_cell && round < max_round);

    free(last_cell_holder);
    return round;
}

Cell* move_next_cell(Labyrinth* this, int origin_x, int origin_y, Cell** last_cell_holder) {
    if (!this || NULL == this) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    if (origin_x < 0 || origin_x >= this->width) { throw_exception("InvalidArgumentException: origin_x"); }
    if (origin_y < 0 || origin_y >= this->height) { throw_exception("InvalidArgumentException: origin_y"); }

    int best_weight = INT_MAX,
        best_direction = 0;
    Cell* current_cell = &this->matrix[origin_x][origin_y];
    Cell* next_cell = NULL;
    Cell* exit_cell = &this->matrix[this->exit->x][this->exit->y];

    if (is_dead_end(this, origin_x, origin_y)) {
        set_cell_weight(current_cell, MAX_CELL_WEIGHT);
    }

    if (_random(101) > 10) {
        // cloclewise
        for(int direction=NORTH; direction<=WEST; ++direction) {
            if (!has_wall(this, current_cell, direction)) {

                Cell* cell_at_dir = cell_at_direction(this, origin_x, origin_y, direction);
                if (NULL == cell_at_dir) {
                    continue; 
                }

                int weight = get_cell_weight(cell_at_dir);
                if (cell_at_dir == *last_cell_holder) {
                    weight += 10;
                }
                if (weight < best_weight || exit_cell == cell_at_dir) {
                    best_weight = weight;
                    best_direction = direction;
                    next_cell = cell_at_dir;

                    if (exit_cell == cell_at_dir) {
                        break;
                    }  
                }
            }
        }
    }
    else {
        // reverse clockwise
        for(int direction=WEST; direction>=NORTH; --direction) {
            if (!has_wall(this, current_cell, direction)) {

                Cell* cell_at_dir = cell_at_direction(this, origin_x, origin_y, direction);
                if (NULL == cell_at_dir) {
                    continue; 
                }

                int weight = get_cell_weight(cell_at_dir);
                if (cell_at_dir == *last_cell_holder) {
                    weight += 10;
                }
                if (weight < best_weight || exit_cell == cell_at_dir) {
                    best_weight = weight;
                    best_direction = direction;
                    next_cell = cell_at_dir;

                    if (exit_cell == cell_at_dir) {
                        break;
                    }  
                }
            }
        }
    }

    *last_cell_holder = &this->matrix[origin_x][origin_y];
    move_walker_to_direction(this, best_direction);
    return next_cell;
}

void move_walker_to_direction(Labyrinth* this, Wall direction) {
    if (!this || NULL == this) { throw_exception("InvalidArgumentException: Labyrinth*"); }

    switch(direction) {
        case NORTH:
            this->walker->y--;
        break;
        case EAST:
            this->walker->x++;
        break;
        case SOUTH:
            this->walker->y++;
        break;

        case WEST:
            this->walker->x--;
        break;
    }
}

int is_dead_end(Labyrinth* this, int x, int y) {
    if (!this || NULL == this) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    if (x < 0 || x >= this->width) { throw_exception("InvalidArgumentException: x"); }
    if (y < 0 || y >= this->height) { throw_exception("InvalidArgumentException: y"); }

    // seald dead end
    Cell* cell =  cell_at_xy(this, x, y);
    int available_path_count = count_available_path(this, cell);

    // Remove dead end from available path count
    for(int direction=NORTH; direction<=WEST; ++direction) {
        Cell* neighboring_cell = cell_at_direction(this, x, y, direction);
        if (neighboring_cell != NULL)
        {
            if (!has_wall(this, cell, direction) &&
                get_cell_weight(neighboring_cell) == MAX_CELL_WEIGHT) {
                available_path_count--;
            }
        }
    }

    if (available_path_count == 1) 
    {
        return 1;
    }
    
    return 0;
}

int count_available_path(Labyrinth* this, Cell* cell)  {
    if (!this || NULL == this) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    if (!cell || NULL == cell) { throw_exception("InvalidArgumentException: Cell*"); }

    int available_path = 4;
    for(int direction=NORTH; direction<=WEST; direction++) {
        if (has_wall(this, cell, direction)) {
            available_path--;
        }
    }
    return available_path;
}

int increment_cell_weight(Cell* cell) {
    if (!cell || NULL == cell) { return -1; }

    return 
        set_cell_weight(
            cell,
            get_cell_weight(cell) + 1);
}

int set_cell_weight(Cell* cell, int weight) {
    if (!cell || NULL == cell) { return -1; }
    if (weight < 0 || weight > MAX_CELL_WEIGHT) { return -2; }

    // set cell weight
    int new_cell_weight = weight;

    // set other bit flag
    for(int i = 15; i >= 11; --i) {
        if (*cell & (1 << i)) {
            new_cell_weight |= (1 << i);
        }
    }

    *cell = new_cell_weight;
    return 1;
}

int get_cell_weight(Cell* cell) {
    if (!cell || NULL == cell) { return -1; }

    int cell_weight = 0x00;

    for(int i = 0; i<=10; ++i) {
        if (*cell & (1 << i)) {
            cell_weight |= (1 << i);
        }
    }
    return cell_weight;
}

Cell* cell_at_xy(Labyrinth* this, int x, int y) {
    if ( (x < 0 || y < 0) ||
         (x >= this->width || y >= this->height)
    ) { throw_exception("InvalidArgumentException: Out of range (x | y)"); }

    return &this->matrix[x][y];
}

// Nullable
Cell* cell_at_direction(Labyrinth* this, int origin_x, int origin_y, Wall direction) {
    if (!this || NULL == this) { throw_exception("InvalidArgumentException: Labyrinth*"); }
    if (origin_x < 0 || origin_y < 0 || origin_x >= this->width || origin_y >= this->height) {
        throw_exception("InvalidArgumentException: Out of range (x | y)");
    }

    switch(direction) {
        case NORTH:
            if (origin_y - 1 < 0) {
                return NULL;
            }
            origin_y--;
        break;
        case EAST:
            if (origin_x + 1 >= this->width) {
                return NULL;
            }
            origin_x++;
        break;
        case SOUTH:
            if (origin_y + 1 >= this->height) {
                return NULL;
            }
            origin_y++;
        break;
        case WEST:
            if (origin_x - 1 < 0) {
                return NULL;
            }
            origin_x--;
        break;
        default:
            return NULL;
        break;
    }

    return &this->matrix[origin_x][origin_y];
}
// #endregion

void labyrinth_destroy(Labyrinth* this) {
    if (!this || NULL == this) { throw_exception("InvalidArgumentException: Labyrinth*"); }

    for(int i=0; i<this->width; ++i){
        free((Cell*) this->matrix[i]);
    }
    free(this->matrix);
    free(this->entry);
    free(this->exit);
    free(this->walker);
    free(this);
}

