#include "dependencies.h"
typedef enum {
    WALKER,
    ENTRY,
    EXIT,
    VISITED,
    DEAD_END
} SpecialState;


void console_labyrinth_renderer(Labyrinth* labyrinth);
void _render_row_walls(Labyrinth *labyrinth, int y);
void _render_cell_special_state(Cell*, int cell_size, SpecialState state);