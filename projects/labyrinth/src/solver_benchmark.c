#include "dependencies.h"

void run_benchmark(int width, int height, int max_round, int run_count, int use_rand_generator) {

	int min = INT_MAX;
	int max = 0;
	int wasted = 0;
	int total = 0;

	for(int i=0; i<run_count; ++i) {
		Labyrinth* labyrinth = labyrinth_new(
    	width, 
    	height, 
    	NULL);

    	if (use_rand_generator) {
    		generate_random(labyrinth, NULL);
    	}
    	else 
    	{
    		generate_perfect(labyrinth, NULL);
    	}
		int round = solve(labyrinth, 0, max_round, NULL);
		if (round == max_round) {
			// unsolved labyrinth, bad generator
			wasted++;
		}
		else {
			if (round < min) {
				min = round;
			}
			if (round > max) {
				max = round;
			}
			total += round;
		}

		printf("runned %d/%d\n", i, run_count);
    	labyrinth_destroy(labyrinth);
	}
	printf("Benchmark for randdom labyrinth %dx%d | %d runs \n", width, height, run_count);
	printf("Wasted : %d\n", wasted);
	printf("Max rounds : %d\n", max);
	printf("Min rounds : %d\n", min);
	printf("Average rounds : %d\n", (total / (run_count - wasted)) );
}