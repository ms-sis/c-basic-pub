#ifndef HEADERS
#define HEADERS

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <limits.h>
#include <error.h>
#include <argp.h>
#include <argz.h>

#include "tools.h"
#include "labyrinth.h"
#include "solver_benchmark.h"
#include "renderer_engine.h"

#endif
