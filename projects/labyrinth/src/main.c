#include "dependencies.h"

const char *argp_program_bug_address = "roose@et.eisea.fr";
const char *argp_program_version = "version 1.0";

static struct arguments
{
    char *argz;
    size_t argz_len;
    int labyrinth_width;
    int labyrinth_height;
    int labyrinth_wasted_round;
    int labyrinth_sleep_timeout;
    int labyrinth_renderer_engine;
    int benchmark_run;
    int labyrinth_use_random_generator;
    int verbose;
    int mode;
    char* labyrinth_file;
} arguments;

static struct argp_option options[] =
{
    {0,0,0,0, "Labyrinth globals options:", 1},
    {"sleep", 's', "NUM", 0, "Time in seconde to wait between each frame"},
    {"random", 'r', 0, OPTION_ARG_OPTIONAL, "Use random generator instead of perfect one"},
    {"renderer-engine", 'e', "NUM", 0, "Enable or disable the console renderer engine"},
    {"wasted", 'w', "NUM", 0, "Number of round before the labyrinth is considered as wasted (unsolvable)"},
    {"input-file", 'f', "STRING", 0, "Load labyrinth from file instead of generate a new one"},
    {"verbose", 'v', 0, OPTION_ARG_OPTIONAL, "Render each step of labyrinth generation"},
    {0,0,0,0, "Labyrinth benchmark options:", 2},
    {"benchmark", 'b', 0, OPTION_ARG_OPTIONAL, "Run benchmark"},
    {"count-run", 'c', "NUM", 0, "Number of benchmark run (default: 1000)"},
    {"konami", 777, 0, OPTION_HIDDEN, "↑↑↓↓←→←→BABA"},
    {0}
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state) {
    struct arguments *input_args = state->input; 
    switch(key) {
        case ARGP_KEY_INIT:
            input_args->argz = 0;
            input_args->argz_len = 0;

            // labyrint default options
            input_args->labyrinth_width = 15;
            input_args->labyrinth_height = 15;
            input_args->labyrinth_wasted_round = 3000;
            input_args->labyrinth_sleep_timeout = 1;
            input_args->labyrinth_use_random_generator = 0;
            input_args->labyrinth_renderer_engine = 1;
            input_args->labyrinth_file = NULL;
            input_args->verbose = 0;
            input_args->mode = 1;

            // benchmark default options
            input_args->benchmark_run = 1000;
        break; 
        case 's':
            input_args->labyrinth_sleep_timeout = atoi(arg);
            break;
        case 'r':
            input_args->labyrinth_use_random_generator = 1;
            break;
        case 'e':
            input_args->labyrinth_renderer_engine = atoi(arg);
            break;
        case 'w':
            input_args->labyrinth_wasted_round = atoi(arg);
            break;
        case 'f':
            input_args->labyrinth_file = arg;
            break;
        case 'v':
            input_args->verbose = 1;
            break;
        case 'b':
            input_args->mode = 2;
            break;
        case 'c':
            input_args->benchmark_run = (atoi(arg) > 0) ? atoi(arg) : 1000;
            break;
        case 777:
            system("telnet towel.blinkenlights.nl");
        break;
        case ARGP_KEY_ARG:
        {
           argz_add (&input_args->argz, &input_args->argz_len, arg);
        }
        break;
        case ARGP_KEY_END:
        {
            size_t count = argz_count (input_args->argz, input_args->argz_len);
            if (count > 2) {
                argp_failure (state, 1, 0, "Too many arguments, use --help");
            }
        }
        break; 
    }
    return 0;
}

int main(int argc, char** argv) {
	// initialize random generator
	srand(time(NULL));

    struct argp argp = { options, parse_opt, "[width] [height]", 
        "\nGenerate random labyrinth (not perfect or perfect) and solve them\nAlso do some basic benchmark stats\v"
        "\"THE BEER-WARE LICENSE\" (Revision 42.1): Nementon wrote this file. As long as you retain this notice you can do whatever you want with this stuff. If we meet some day, and you think this stuff is worth it, you can buy me a beer in return."};

    printf(" ------ [Labyrinth] ------\n\n");
    

    if (argp_parse (&argp, argc, argv, 0, 0, &arguments) == 0) {

        const char *prev = NULL;
        char *word;
        int pos = 0;
        while ((word = argz_next(arguments.argz, arguments.argz_len, prev)))
        {
            int size = (atoi(word)) > 0 ? atoi(word) : 15;
            if (pos == 0) {
                arguments.labyrinth_width = size;
            }
            else {
                arguments.labyrinth_height = size;
            }
            pos++;
            prev = word;
        }
        
        Action renderer_engine =  arguments.labyrinth_renderer_engine == 1 ? &console_labyrinth_renderer : NULL;
        Action generator_renderer_engine = arguments.verbose == 1 ? &console_labyrinth_renderer : NULL;
        switch(arguments.mode) {
            case 1:
            {
                Labyrinth* labyrinth = NULL;
                if (arguments.labyrinth_file != NULL) {
                   labyrinth = labyrinth_unserialize(arguments.labyrinth_file);
                }
                else {
                    labyrinth = labyrinth_new(
                        arguments.labyrinth_width, 
                        arguments.labyrinth_height,
                        renderer_engine);

                    if (arguments.labyrinth_use_random_generator) {
                        generate_random(labyrinth, generator_renderer_engine);
                    }
                    else {
                        generate_perfect(labyrinth, generator_renderer_engine);
                    }
                }

                int result =
                solve(
                    labyrinth, 
                    arguments.labyrinth_sleep_timeout, 
                    arguments.labyrinth_wasted_round, 
                    renderer_engine);

                if (result != arguments.labyrinth_wasted_round) {
                    printf("Solved in %d round !\n", result);
                }
                else {
                    printf("Wasted !\n");
                }

                labyrinth_destroy(labyrinth);
            }
            break;
            case 2:
            run_benchmark(
                arguments.labyrinth_width, 
                arguments.labyrinth_height, 
                arguments.labyrinth_wasted_round, 
                arguments.benchmark_run,
                arguments.labyrinth_use_random_generator);
            break;
        }
        free(arguments.argz);
    }
    return 0;
}
