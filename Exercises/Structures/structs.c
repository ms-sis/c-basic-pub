#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct vehicle {
    char* manufacturer;
    int max_speed;
    int power;
};

typedef struct vehicle Car;

int random(int min, int max) {
    if (min > max || min < 0) {
        return -1;
    }

    return rand()%(max - min)+min;
}

Car build_vehicle() {
    char vehicles_def[4][128] = { "Renault", "Peugeot", "Audi", "BMW" };
    char* r_manufacturer = vehicles_def[random(0,3)];
    Car vehicle;

    char* manufacturer = calloc(strlen(r_manufacturer)+1, sizeof(char));
    strcpy(manufacturer, r_manufacturer);

    vehicle.manufacturer = manufacturer;
    vehicle.max_speed = random(180, 400);
    vehicle.power = random(100, 700);

    return vehicle;
}

void init_vehicle(Car* vehicle) {
    Car new_car = build_vehicle();

    *vehicle.manufacturer = new_car.manufacturer;
    *vehicle.max_speed = new_car.max_speed;
    *vehicle.power = new_car.power;
}

void debug_vehicle(Car vehicle) {
    printf("[Vehicle] > manufacturer %s, power: %d, max speed: %d \n",
            vehicle.manufacturer, vehicle.power, vehicle.max_speed);
}

int main(void) {
    srand(time(NULL));

    Car vehicle;
    vehicle = build_vehicle();
    debug_vehicle(vehicle);

    Car* p_vehicle = calloc(1, sizeof(Car));
    init_vehicle(p_vehicle);
    debug_vehicle(*p_vehicle);
}
