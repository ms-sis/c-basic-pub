#include <stdio.h>

int length(char* string) {
	int s_length = 0;
	for(s_length = 0; string[s_length] != '\0'; ++s_length);
	return s_length;
}

void bits(char* string) {
	for(int i = 0, s_length = length(string); i < s_length; ++i) {
		char c = string[i];
		printf("Ascii : %c > Ascii value : %d > binary value : ", c, (int)c);
		for(int j = 7; j >=0; j--) {
			printf("%s", (c & (1 << j)) ? "1" : "0");
		}
		printf("\n");
	}
}

char* flip(char * string) {
	for(int i = 0, s_length = length(string); i < s_length; ++i) {
		string[i] = ~string[i];
		// Fix 8em bit, force to set it to 0;
    	string[i] &= ~(1<<7);
	}
	return string;
}

char* flip_with_mask(char* string) {
	int mask = 0x7F;
	for(int i = 0, s_length = length(string); i < s_length; ++i) {
    	string[i] = string[i] ^ mask;
	}
	return string;
}

char* permute_bit_fort_faible(char * string) {
	for(int i = 0, s_length = length(string); i < s_length; ++i) {
		char c = string[i];
		char reverse = 0x00;
		
		for (int i = 0 ; i<7; ++i)
		{
        	reverse <<= 1;
        	reverse |= (c & (1 << i)) >> i;   
		}
		
		for(int j = 7; j >=0; j--) {
			printf("%s", (reverse & (1 << j)) ? "1" : "0");
		}
		printf("\n");
	}
	return string;
}

void print_sbinary_to_string(char *string) {
	char s_converted[5000] = { '\0' };
	for(int i = 0, s_pos = 0, s_length = length(string); i < s_length; i += 8, ++s_pos) {
		char c_converted = 0x00;
		for(int j = 0; j<=7; ++j) {
			char c_binary_value = string[i + j];
			if (c_binary_value == '1') {
				c_converted |= (1<< 7 - j);
			}
		}
		s_converted[s_pos] = c_converted;
	}
	printf("Converted binary string to ascii string is : %s \n", s_converted);
}

void affichage_texte() {
	//char my_string[5000] = {'\0'};
	//int scanf_result = 0;
	//do {
	//	scanf_result = scanf("Please, entrer a binnary string (4999 chars max) >" %4999s", my_string);
	//} while(scanf_result != 1);
	
	// For test, string > "Hello w0rld!"
	char my_string[5000] = "01001000011001010110110001101100011011110010000001110111001100000111001001101100011001000010000000100001";
	print_sbinary_to_string(my_string);
}

int main(void) {
	char my_string[] = "abcdefABCDEF";
	
	printf("Original string is %s \n\n", my_string);
	
	bits(my_string);
 	printf("Flip string (with bit mask) is : %s \n", flip_with_mask(my_string));
	printf("Flip of flip string (with ~ operator) is : %s \n", flip(my_string));
	permute_bit_fort_faible(my_string);
	affichage_texte();
	return 0;
}
