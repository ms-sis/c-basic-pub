#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>

void variable_by_ref(int *value) {
    printf("Int value in function %d \n", *value);
    *value = 42;
    printf("Int value in fucntion after modificaiton %d \n", *value);
}

void affiche_n(int *array, int count) {
    int *p_array = array;
    for(int i = 0; i < count; ++i, p_array += 1) {
        printf("Array %d : %d \n", i, (*p_array));
    }    
}

void passe_valeur(int value) {
    printf("The value is %d \n", value);
}

void passe_address(int *array) {
    affiche_n(array, 5);
}

int* alloc_array() {
    int scanf_result = 0;
    int cell_count = 0;
    do {
        printf("How many cell do you want to allocate ? : ");
        scanf_result = scanf("%d", &cell_count);
    } while(scanf_result != 1 && cell_count <= 0);

    int* array = (int*) calloc(cell_count, sizeof(int));
    int* p_array = array;
    for(int i = 0;  i<cell_count; ++i, ++p_array) {
        if (i - 15 < 94) {
            (*p_array) = 15 + i;
        }
        else {
            (*p_array) = 94;
        }
    }
    return array;
}

int** alloc_mat(int nb_l, int nb_c) {
    int** matrice = (int**) calloc(nb_l, sizeof(int*));
    int** c_line = matrice;
    for(int i = 0; i < nb_l; ++i, ++c_line) {
        (*c_line) = (int*) calloc(nb_c, sizeof(int));
    }
    return matrice;
}

void lib_mat(int** matrice, int nb_l) {
    int** c_line = matrice;
    for(int i = 0; i<nb_l; ++i, ++c_line) {
        int* col = (*c_line);
        free(col);
    }
    free(matrice);
}


void remp_mat(int** matrice, int nb_l, int nb_c) {
    for(int i = 0; i<nb_l; ++i) {
        for(int j=0; j<nb_c; ++j) {
            matrice[i][j] = (i + 1) + (j + 2);
        }
    }
}

void affiche_mat(int** matrice, int nb_l, int nb_c) {
    for(int i=0; i<nb_l; ++i) {
        for(int j=0; j<nb_c; ++j) {
            printf("[%d-%d > %d]", i, j, *(*(matrice+i)+j));
        }
        printf("\n");
    }
}

void test_mat() {
    int scanf_result = 0;
    int height = 0;
    int width = 0;
    do {
        printf("Select a height and width for the matrice : ");
        scanf_result = scanf("%d%d", &height, &width);
    } while(scanf_result != 2 && height <= 0 && width <= 0);

    int** matrice = alloc_mat(height, width);
    remp_mat(matrice, height, width);
    affiche_mat(matrice, height, width);
    lib_mat(matrice, height);
}

void affiche_mat_ds_fic(int** matrice, int nb_l, int nb_c) {
    FILE* file = NULL;
    if ( (file = fopen("matrice.txt", "w")) != NULL ) {
        for(int i=0; i<nb_l; ++i) {
            for(int j=0; j<nb_c; ++j) {
                fprintf(file, "%d\t", *(*(matrice+i)+j) );
            }
            fprintf(file, "\n");
        }

        fclose(file);
    }
    else {
        abort();
    }
}


//void remp_mat_via_fic(int** matrice, int nb_l; int nb_c) {
//    FILE* file = NULL;
//    if ( (file = fopen("matrice.txt", "r")) != NULL ) {
//        char* csvToken;
//        char line[5024];
//        int tokenPos = 0;
//
//        while(!feof(file)) {
//            fgets(line);
//            cvsToken = strtok(line, "\t");
//        }
//
//        for(int i=0; i<nb_l; ++i) {
//            for(int j=0; j<nb_c; ++j) {
//                matrice[i][j] = atoi( *(csvToken) );
//                csvToken++;
//            }
//        }
//    }
//}


int get_file_lines_count(char* filename) {
    FILE* file = NULL;
    char* line = NULL;
    int line_count = 0;
    size_t length = 0;

    if ( (file = fopen(filename, "r")) != NULL) {
        while(getline(&line, &length, file) != -1) {
            line_count++;
        }
        return line_count;
    }
    return -1;
}

int main(void) {

    int value = 0;
    printf("Int value in main %d \n", value);
    variable_by_ref(&value);
    printf("Int value in main after func call %d \n", value);
    
    int *array = (int*) calloc(22, sizeof(int));
    int *array_head = array;
    for (int i=0; i<22; ++i, array+=1) {
        (*array)= i + 5;
    }
    affiche_n(array_head, 5);
    affiche_n(array_head, 1);

    int *p_array = array_head;
    p_array += 5;

    passe_valeur( (*p_array) );
    passe_address(p_array);

    free(array_head);

    int* user_array = alloc_array();
    
    int cell_pos = -1;
    int scanf_result = 0;
    do {
        printf("Which cell do you want to show ? : ");
        scanf("%d", &cell_pos);
    } while(scanf_result != 1 && cell_pos < 0);

    printf("Cell content > %d \n", (*(user_array + cell_pos)));   
    free(user_array);

    
    int** matrice = alloc_mat(5,5);
    remp_mat(matrice, 5,5);
    affiche_mat(matrice, 5,5);
    affiche_mat_ds_fic(matrice, 5,5);
    lib_mat(matrice, 5);

    test_mat();

    printf("Line count in matrice.txt > %d \n", get_file_lines_count("matrice.txt"));
}
