#include <stdio.h>

int is_digit(char c) {
	int asciiValue = (int)c;
	return asciiValue >= 48 && asciiValue <= 57 ? 1 : 0;
}

int is_character(char c) {
	int asciiValue = (int)c;
	return (asciiValue >= 65 && asciiValue <= 90 ) || 
			( asciiValue >= 97 && asciiValue <= 122 )
			? 1
			: 0;
}

int length(char* string) {
	int s_length = 0;
	for(s_length = 0; string[s_length] != '\0'; ++s_length);
	return s_length;
}

int digits_count(char* string) {
	int digit_count = 0;
	for (int i = 0, s_length = length(string); i < s_length; ++i) {
		if (is_digit(string[i]) == 1) {
			digit_count++;
		}
	}
	return digit_count;
}

int characters_count(char* string) {
	int char_count = 0;
	for (int i = 0, s_length = length(string); i < s_length; ++i) {
		if (is_character(string[i]) == 1) {
			char_count++;
		}
	}
	return char_count;
}

int special_characters_count(char* string) {
	return length(string) - digits_count(string) - characters_count(string);
}

char* reverse_string(char* string) {
	char tmp;
	for(int i = 0, s_length = length(string); i < s_length / 2; ++i) {
		tmp = string[i];
		string[i] = string[s_length - (i + 1)];
		string[s_length - (i + 1)] = tmp;
	}
	return string;
}

int find_char_pos(char* string, int seekChar) {
	for(int i = 0, s_length = length(string); i < s_length; ++i) {
		if ((int)string[i] == seekChar) {
			return i;
		}
	}
	return -1;
}

int main(void) {
	char my_string[] = "abcdef5235dszedffg-+*/m";
	
	printf("String length is %d \r", length(my_string));
	printf("String digits_count count is %d \r", digits_count(my_string));
	printf("String characters_count count is %d \r", characters_count(my_string));
	printf("String special characters_count count is %d \r", special_characters_count(my_string));
	printf("The frist position of char 'a' is %d \r", find_char_pos(my_string, 'a'));
	printf("The first position of char '2' is %d \r", find_char_pos(my_string, '2'));
	printf("The first position of char '9' is %d \r", find_char_pos(my_string, '9'));
	
	printf("Original string is %s \r", my_string);
	printf("String reverse_string is %s \r", reverse_string(my_string));
	return 0;
}
